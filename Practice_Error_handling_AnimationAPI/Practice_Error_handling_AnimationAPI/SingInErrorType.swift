//
//  SingInErrorType.swift
//  Practice_Error_handling_AnimationAPI
//
//  Created by Jorge luis Menco Jaraba on 7/28/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation

enum SingInValidationError: Error {
    case usernameLong
    case passwordNotHasLetter
    case passwordShort
}

extension SingInValidationError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .usernameLong:
            return NSLocalizedString("SingInError.UsernameLong", comment: "")
        case .passwordShort:
            return NSLocalizedString("SingInError.PasswordShor", comment: "")
        case .passwordNotHasLetter:
            return NSLocalizedString("SingInError.PasswordNotHadLetter", comment: "")
        }
    }
}
