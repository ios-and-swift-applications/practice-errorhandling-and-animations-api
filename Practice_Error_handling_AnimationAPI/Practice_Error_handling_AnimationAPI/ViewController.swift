//
//  ViewController.swift
//  Practice_Error_handling_AnimationAPI
//
//  Created by Jorge luis Menco Jaraba on 7/28/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    private struct InnerConstans {
        static let maximunLongUsername = 15
        static let minimunLongPassword = 4
    }
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var PasswordTextfield: UITextField!
    @IBOutlet weak var animationtextField: UITextField!
    @IBOutlet weak var animationUIVIew: UIView!
    @IBOutlet weak var animationButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func didTapInSingInButton(_ sender: Any) {
        validateUsernameAndPassword()
    }
    
    @IBAction func startAnimations(_ sender: Any) {
        animateView()
        animateTextField()
    }
    
    private func validateUsernameAndPassword() {
        guard let username = usernameTextField.text,
            let password = PasswordTextfield.text else {
                return
        }
        do {
            try isUsernameLong(Username:username)
            try varifyPasswordHasSomeLetter(username: username)
            try isShortPassword(password: password)
            showAlert(message: "Ingreso correcto")
            
        } catch {
            showAlert(message: error.localizedDescription)
        }
    }
    
    private func varifyPasswordHasSomeLetter(username: String) throws {
        try username.forEach({
            guard $0.isLetter else {
                throw SingInValidationError.passwordNotHasLetter
            }
        })
    }
    
    private func isUsernameLong(Username: String) throws {
        if  Username.count > InnerConstans.maximunLongUsername {
            throw SingInValidationError.usernameLong
        }
    }
    
    private func isShortPassword(password: String) throws {
        if password.count < InnerConstans.minimunLongPassword {
            throw SingInValidationError.passwordShort
        }
    }
    
    private func showAlert(message: String) {
        let alertMessage = UIAlertController(title: "Error",
                                             message: message,
                                             preferredStyle: .alert)
        alertMessage.addAction(UIAlertAction(title: "OK",
                                             style: .default,
                                             handler: nil))
        
        present(alertMessage, animated: true)
    }
    
    private func animateTextField() {
        UIView.animate(withDuration: 0.4, animations: {
            self.animationtextField.center.x += 10
            self.animationtextField.frame.size.height = 20
            self.animationtextField.backgroundColor = UIColor.blue
        })
    }
    
    private func animateView() {
        let animation = UIViewPropertyAnimator(
            duration: 0.4,
            curve: .easeIn, animations: {
                self.animationUIVIew.layer.masksToBounds = true
                self.animationUIVIew.layer.cornerRadius = 5.0
                self.animationUIVIew.frame.size.width = 150
                self.animationUIVIew.frame.size.height = 150
        })
        
        animation.startAnimation()
    }
}

extension ViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
}
